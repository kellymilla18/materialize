drop database altarejos;
create database altarejos;
use altarejos;

# Agent Table
create table if not exists registrant(
    registrant_id int auto_increment not null,
    first_name varchar(250) not null,
    middle_name varchar(250) not null,
    last_name varchar(250) not null,
    contact_number varchar(50) not null,
    sex varchar(10) not null,
    email_address varchar(250) not null,
    birthdate date not null,
    occupation varchar(250) not null,
    interviewed boolean DEFAULT false,
    primary key(registrant_id)
);

create table if not exists admin(
	admin_id int auto_increment not null,
	username varchar(150) not null unique,
	password varchar(150) not null,
	primary key(admin_id)
);

create table if not exists school(
    school_id int auto_increment not null,
    school_name varchar(100) not null,
    school_level int not null,
    primary key(school_id)
);

create table if not exists recruits(
	recruit_id int auto_increment not null,
	recruit_fname varchar(50) not null,
	recruit_minit varchar(5) not null,
	recruit_lname varchar(50) not null,
	primary key(recruit_id)
);

create table if not exists events(
    event_id int not null auto_increment,
    event_name varchar(50) not null,
    event_image varchar(150) not null,
    event_description text not null,
    start_event varchar(50) not null,
    end_event varchar(50) not null,
    time_stamp datetime not null,
    primary key(event_id)
);

create table if not exists activities(
	activity_id int not null auto_increment,
	activity_title varchar(100) not null,
	activity_description text not null,
	activity_image varchar(150) not null,
    time_stamp datetime not null,
	primary key(activity_id)
);

create table if not exists about(
	mission varchar(250) not null,
	vision varchar(250) not null,
	email_address varchar(150) not null,
	facebook_page varchar(250) not null
);

create table if not exists contact(
	contact_id int auto_increment not null,
	district varchar(150) not null,
	number varchar(150) not null,
	primary key(contact_id)
);

create table if not exists activity_repo(
    repo_id int auto_increment not null,
    activity_image varchar(150) not null,
    activity_id int not null,
    primary key(repo_id),
    foreign key(activity_id) references activities(activity_id)
);

# Associative Entity
create table if not exists school_agent(
    graduation_date date not null,
    school_id int not null,
    foreign key(school_id) references school(school_id)
);