use altarejos;

insert into admin (username, password) values ('altarejos', md5('starshine'));

insert into about(mission, vision, email_address, facebook_page) values ('To Create and Build a Winning and Happy Culture.', 'Help Every advisor to achieve their personal goals as well as the financial goal of their clients.', 'philamlifealtarejosagency@yahoo.com', 'https://www.facebook.com/Philam-Life-Altarejos-Agency-446334508801359/?fref=ts');

insert into contact (district, number) values ('Naga District', '(473-9807)');
insert into contact (district, number) values ('Iriga District', '(228-0684)');
insert into contact (district, number) values ('Daet District', '(721-0122)');
insert into contact (district, number) values ('Legaspi District', '(460-7661)');
