<?php

class Event_Model extends CI_Model {
	public function addEvent($data) {
		$this->db->insert('events',$data);
		return mysql_insert_id();
	}

	public function countEvents(){
		return $this->db->count_all('events');
	}

	public function getEvents() {
		return $this->db->get('events');
	}

	public function deleteevent($event_id) {
		$this->db->where('event_id', $event_id);
		$this->db->delete('events');
	}

	public function updateevent($event_id, $data) {
		$this->db->where('event_id', $event_id);
		$this->db->update('events', $data);
	}
	
	public function updateEventImage($event_id, $path) {
		$this->db->where('event_id', $event_id);
		$data['event_image'] = $path;
		$this->db->update('events', $data);
	}

	public function latest(){
		$this->db->order_by('time_stamp', 'DESC');
		$query = $this->db->get('events');
		$row = $query->result_array();
		return $row;
	}
}