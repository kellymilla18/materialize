<?php

class Registrant_Model extends CI_Model {
	public function addRegistrant($data) {
		$this->db->insert('registrant', $data);
	}

	public function getRegistrants() {
		return $this->db->get('registrant');
	}
}