<?php

class Activity_Model extends CI_Model {
	public function addActivities($data) {
		$this->db->insert('activities', $data);
		return mysql_insert_id();
	}

	public function addtorepo($data){
		$this->db->insert('activity_repo', $data);
	}

	public function countActivities() {
		return $this->db->count_all('activities');
	}

	public function getActivities() {
		return $this->db->get('activities');
	}

	public function deleteactivity($activity_id) {
		$this->db->where('activity_id', $activity_id);
		$this->db->delete('activities');
	}

	public function updateActivities($activity_id, $data) {
		$this->db->where('activity_id', $activity_id);
		$this->db->update('activities', $data);
	}

	public function updateactivityimage($activity_id, $path) {
		$data['activity_image'] = $path;
		$this->db->where('activity_id', $activity_id);
		$this->db->update('activities', $data);
	}

	public function latest(){
		$this->db->order_by('time_stamp', 'DESC');
		$query = $this->db->get('activities');
		$row = $query->result_array();
		return $row;
	}

	public function activityByID($activity_id){
		$this->db->where('activity_id', $activity_id);
		$query = $this->db->get('activity_repo');
		$row = $query->result_array();
		return $row;
	}

	public function getActivityByID($activity_id){
		$this->db->where('activity_id', $activity_id);
		$query = $this->db->get('activities');
		$row = $query->result_array();
		return $row;
	}
	
	public function addImage($activity_id) {
		$data['activity_id'] = $activity_id;
		$data['activity_image'] = "";
		$this->db->insert('activity_repo', $data);
		return mysql_insert_id();
	}

	public function updateImage($activity_id, $image_id, $path) {
		$data['activity_image'] = $path;
		$this->db->where('repo_id', $image_id);
		$this->db->where('activity_id', $activity_id);
		$this->db->update('activity_repo', $data);
	} 

	public function getLast($activity_id) {
		$this->db->where('activity_id', $activity_id);
		$this->db->order_by('repo_id', 'DESC');
		$this->db->limit(1);
		return $this->db->get('activity_repo');
	}
}