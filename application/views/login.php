  <div class="row">
    <div class="col s12 l8 m10 offset-l2 offset-m1" style="margin-top: 100px; margin-bottom: 20px; background: rgba(255,0,0,0.08); padding-top: 30px; padding-bottom: 30px">
      <div class="row">
        <form id="login-form" class="col l4 m6 offset-l4 offset-m3 s12" method="post">
          <div class="row">
            <div class="input-field col s12">
              <input id="username" type="text" name="username">
              <label for="username">Admin Username</label>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s12">
              <input id="password" type="password" name="password">
              <label for="password">Admin Password</label>
            </div>
          </div>
          <div class="row">
            <button id="login-btn" class="red lighten-2 btn waves-effect waves-light col s12">Log in</button>
          </div>
        </form>
      </div>
    </div>
  </div>