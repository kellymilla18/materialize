  <div class="row" style="padding-top: 100px">
    <div class="row col s12 m10 l10 offset-m1 offset-l1">
    	<div class="col s10 m8 l8 offset-s1 offset-m2 offset-l2">
    		<form id="register-form" action="<?php echo base_url('index.php/pages/registerAttempt'); ?>" method="post">
    			<div class="row">
            <div class="input-field col s12 m4 l4">
              <input id="first-name" type="text" name="first_name">
              <label for="first-name">First Name</label>
            </div>
            <div class="input-field col s12 m4 l4">
              <input id="middle-name" type="text" name="middle_name">
              <label for="middle-name">Middle Name</label>
            </div>
            <div class="input-field col s12 m4 l4">
              <input id="last_name" type="text" name="last_name">
              <label for="last_name">Last Name</label>
            </div>

            <div class="input-field col s12 m12 l12">
              <input id="contact-number" type="text" name="contact_number">
              <label for="contact-number">Contact Number</label>
            </div>
            
						<div class="input-field col s3 m3 l3">
              <input id="gender-male" type="radio" value="Male" name="gender">
              <label for="gender-male">Male</label>
            </div>

						<div class="input-field col s3 m3 l3">
              <input id="gender-female" type="radio" value="Female" name="gender">
              <label for="gender-female">Female</label>
            </div>

            <div class="input-field col s12 m12 l12" style="margin-top: 30px;">
              <input id="birthdate" type="date" class="datepicker" name="birthdate">
              <label for="birthdate">Birthdate</label>
            </div>

            <div class="input-field col s12 m12 l12">
              <input id="occupation" type="text" name="occupation">
              <label for="occupation">Occupation</label>
            </div>

            <div class="input-field col s12 m12 l12">
              <input id="email" type="email" name="email">
              <label for="email">Email</label>
            </div>
            <div class="input-field col s3 m3 l3 offset-s3 offset-m3 offset-l3">
              <a href="<?php echo base_url('index.php/pages/home'); ?>" class="btn-flat waves-effect waves-red">Cancel</a>
            </div>
            <div class="input-field col s3 m3 l3">
              <a id="register-btn" class="btn-flat waves-effect waves-red">Register</a>
            </div>
          </div>

    		</form>
    	</div>
    </div>
  </div>
