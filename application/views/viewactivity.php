<div class="row" style="padding-top: 100px">
	<h4 style="text-align: center;"><?php foreach($detail as $row){ echo $row['activity_title']; } ?></h4>
	<div style="text-align: center; margin-bottom: 30px;"><?php foreach($detail as $row){ echo $row['activity_description']; } ?></div>
	<div class="col s12 m10 l10 offset-m1 offset-l1">
		<div class="row">
			<?php foreach ($images as $img) { ?>
			<div class="col s12 m4">
				<div class="card hoverable">
					<div class="card-image">
						<img class="materialboxed image-gallery" src="<?php echo base_url($img['activity_image']); ?>">
					</div>
				</div>
			</div>
			<?php } ?>
		</div>
	</div>
	<?php if($admin == 1) { ?>
		<div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
			<a id="add-activity-img"
				data-target="image-modal"
				data-action="<?php echo base_url('index.php/pages/imagerepo'); ?>"
				class="btn modal-trigger btn-floating btn-large red">
				<i class="large material-icons">add</i>
			</a>
		</div>

		<!-- Modal Structure -->
		<div id="image-modal" class="modal modal-fixed-footer">
			<div class="modal-content">
				<h4 id="modal-header-title">Add Image</h4>
				<hr>
				<form id="add-img-form" method="post" action="<?php echo base_url('index.php/pages/imagerepo'); echo "/"; foreach($detail as $row){ echo $row['activity_id']; }?>" enctype="multipart/form-data">
					<div class="file-field input-field">
						<div class="btn">
							<span>Image</span>
							<input type="file" name="piks[]" accept="image/*" multiple>
						</div>
						<div class="file-path-wrapper">
							<input class="file-path" type="text" name="file_path">
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Close</a>
				<a href="#!" id="add-finalize" class="modal-action modal-close waves-effect waves-green btn-flat">Add</a>
			</div>
		</div>
	<?php } ?>
</div>