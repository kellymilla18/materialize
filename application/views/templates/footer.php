		</div>
		<footer class="page-footer" style="padding: 0px">
          <div class="footer-copyright">
            <div class="container">
            	<center>
            		Copyright © Altarejos Agency's Website <?php echo date('Y'); ?>
            	</center>
            </div>
          </div>
        </footer>
        <script src="<?php echo base_url('assets/js/controls.js'); ?>"></script>
    </body>
</html>