  <center>
  <div class="col s12 m12 l12 brand-container">
    <div class="brand-header">
      <img src="<?php echo base_url('assets/images/logo.png'); ?>" height="200">
    </div>
    <div class="address-bar">
      PENAFRANCIA | NAGA, BICOL | PHILIPPINES
    </div>
  </div>
  </center>
  <nav class="navi-top">
      <div class="nav-wrapper">
        <ul id="nav-mobile" class="left">
          <li><a href="<?php echo base_url('index.php/pages/home'); ?>">Home</a></li>
          <li><a href="<?php echo base_url('index.php/pages/activities'); ?>">Activities</a></li>
          <li><a href="<?php echo base_url('index.php/pages/events'); ?>">Events</a></li>
          <li><a href="<?php echo base_url('index.php/pages/about'); ?>">About</a></li>
          <?php if($admin == 1) { ?> 
          <li><a href="<?php echo base_url('index.php/pages/registrants'); ?>">Registrants</a></li>
          <?php } ?>
        </ul>      
        <ul class="right">
          <?php if($admin == 1) { ?> 
            <li><a href="<?php echo base_url('index.php/pages/logout'); ?>">Logout</a></li>
          <?php } else { ?>
            <li><a href="<?php echo base_url('index.php/pages/login'); ?>">Login</a></li>
          <?php } ?>
        </ul>
      </div>
  </nav>

    <div style="min-height: 484px">