<html>
	<head>
		<title>Altarejos Agency</title>
		<link href="<?php echo base_url('assets/images/favicon.ico'); ?>" rel="icon" type="image/x-icon" />
		<link href="<?php echo base_url('assets/materialize/css/material-icons.css'); ?>" rel="stylesheet">
		<link href="<?php echo base_url('assets/materialize/css/materialize.css'); ?>" type="text/css" rel="stylesheet" media="screen,projection"/>
		<link href="<?php echo base_url('assets/materialize/css/style.css'); ?>" type="text/css" rel="stylesheet" media="screen,projection"/>


		<script src="<?php echo base_url('assets/js/jquery-2.1.1.min.js'); ?>"></script>
		<script src="<?php echo base_url('assets/materialize/js/materialize.js'); ?>"></script>
		<script src="<?php echo base_url('assets/materialize/js/init.js'); ?>"></script>
	</head>
	<body style="background: white">