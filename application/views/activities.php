  <div class="row" style="padding-top: 100px">
    
    <div class="col s12 m10 l10 offset-m1 offset-l1">
      <div class="slider">
        <ul class="slides">
          <?php for($x = 0; $x < count($activity); $x++) { ?>
          <li>
            <img src="<?php echo base_url($activity[$x]['activity_image']); ?>"> <!-- random image -->
            <div class="caption <?php if($x%3==0) echo "center"; else if($x%3==1) echo "left"; else echo "right"; ?>-align">
              <h3><?php echo $activity[$x]['activity_title']; ?></h3>
              <h5 class="light grey-text text-lighten-3"><?php echo $activity[$x]['activity_desc']; ?></h5>
            </div>
          </li>
          <?php } ?>
        </ul>
      </div>

      <div class="row" style="margin-top: 50px;">
      <h4>Starshine Altarejos Agency</h4><hr>
         <?php for($x = 0; $x < count($activity); $x++) { ?>
            <div class="col s12 m6 l4">
              <div class="card hoverable">
                <div class="card-image">
                    <img class="materialboxed image-gallery" data-caption="<?php echo $activity[$x]['activity_desc']; ?>" src="<?php echo base_url($activity[$x]['activity_image']); ?>">
                <div class="card-title" style="padding:0px 10px 10px 10px; width: 100%; background-color: rgba(100,10,10,0.5);"><a href="<?php echo base_url('index.php/pages/viewactivity/' . $activity[$x]['activity_id']); ?>" style="text-decoration: none"><?php echo $activity[$x]['activity_title']; ?></a></div>
                </div>
                <div class="card-content" style="height:150px; overflow:hidden">
                  <p><?php echo $activity[$x]['activity_desc']; ?></p>
                </div>
                <div class="row " style="padding: 5px 5px 5px 5px">
                  <?php if($admin == 1) { ?>
                
                  <div class="row col s6 m6 l6 center">
                    <a data-target="modal1" 
                       data-activitytitle="<?php echo $activity[$x]['activity_title']; ?>"
                       data-activitydesc="<?php echo $activity[$x]['activity_desc']; ?>"  
                       data-activityid="<?php echo $activity[$x]['activity_id']; ?>"
                       data-action="<?php echo base_url('index.php/pages/updateactivity'); ?>"

                       class="edit-activity-btn col s12 m12 l12 modal-trigger waves-effect waves-green btn-flat">
                      <i class="small material-icons">edit</i>
                      Edit
                    </a>
                  </div>
                  <div class="row col s6 m6 l6 center">
                    <a data-target="modal-delete" data-actid="<?php echo $activity[$x]['activity_id']; ?>"class="delete-activity-btn col s12 m12 l12 modal-trigger waves-effect waves-green btn-flat">
                      <i class="small material-icons">delete</i>
                      Delete
                    </a>
                  </div>
                  <?php } ?>
                </div>
              </div>
            </div>
        <?php } ?>
      </div>
    </div>
  </div>

  
  <?php if($admin == 1) { ?>
  <div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
    <a id="add-activity-float" 
       data-action="<?php echo base_url('index.php/pages/addactivity'); ?>"
       data-target="modal1" 
       class="btn modal-trigger btn-floating btn-large red">
      <i class="large material-icons">add</i>
    </a>
  </div>


  <!-- Modal Structure -->
  <div id="modal1" class="modal modal-fixed-footer">
    <div class="modal-content">
      <h4 class="modal-activity-header">Add Activities</h4>
      <hr>
      <form id="add-activity-form" method="post" action="" enctype="multipart/form-data">
          <input type="text" hidden name="activity_id" value="">
          <div class="row">
            <div class="input-field col s12">
              <input id="activity-name" type="text" name="activity_name">
              <label for="activity-name">Activity Name</label>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s12">
              <textarea id="act-desc" type="text" name="activity_desc" class="materialize-textarea"></textarea>
              <label for="act-desc">Activity Description</label>
            </div>
          </div>

          <div class="file-field input-field">
            <div class="btn">
              <span>Image</span>
              <input type="file" name="piks" accept="image/*">
            </div>
            <div class="file-path-wrapper">
              <input class="file-path" type="text" name="file_path">
            </div>
          </div>

      </form>
    </div>
    <div class="modal-footer">
      <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat">Close</a>
      <a href="#!" id="add-activity-btn" class=" modal-action modal-close waves-effect waves-green btn-flat">Add</a>
    </div>
  </div>

  <div id="modal-view" class="modal modal-fixed-footer">
    <div class="modal-content">
      <h4 class="modal-activity-header">Activity Images</h4>
      <hr>
    </div>
    <div class="modal-footer">
      <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat">Close</a>
      <a data-target="modal-add-img" id="add-repo-act-btn" class="modal-trigger waves-effect waves-green btn-flat">Add</a>
    </div>
  </div>

  <div id="modal-add-img" class="modal modal-fixed-footer">
    <div class="modal-content">
      <h4 class="modal-activity-header">Add Activity Image</h4>
      <hr>
      <form id="add-activity-form" method="post" action="" enctype="multipart/form-data">
        <div class="file-field input-field">
          <div class="btn">
            <span>Image</span>
            <input type="file" name="piks" accept="image/*">
          </div>
          <div class="file-path-wrapper">
            <input class="file-path" type="text" name="file_path">
          </div>
        </div>
      </form>
    </div>
    <div class="modal-footer">
      <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat">Close</a>
      <a href="#!" id="add-repo-act-btn" class="modal-action modal-close waves-effect waves-green btn-flat">Add</a>
    </div>
  </div>

  <div id="modal-delete" class="modal">
    <div class="modal-content">
      <h6>Notice</h6>
      <hr>
      <form id="delete-activity-form" action="<?php echo base_url('index.php/pages/deleteactivity') ?>" method="post">
        <input type="text" id="del-act-id" name="delete-activity-id" hidden value="">
      </form>
      Are you sure you want to delete this activity?
    </div>
    <div class="modal-footer">
      <a class=" modal-action modal-close waves-effect waves-green btn-flat">Cancel</a>
      <a id="delete-actconfirm-btn" class=" modal-action modal-close waves-effect waves-green btn-flat">Delete</a>
    </div>
  </div>
  <?php } ?>