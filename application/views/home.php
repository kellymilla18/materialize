<div class="row" style="margin-top: 100px">
	<div class="col	s12 m10 l10 offset-l1 offset-m1">
		<div class="row">
			<div class="col s12 m12 l12">
				<center><p style="font-size: 20px;">Be part of the Philam Life Altarejos Agency! <a href="<?php echo base_url('index.php/pages/register'); ?>">Register here</a></p></center>
			</div>
		</div>
		<div class="row">
			<!-- Events preview -->
			<h4>Events</h4>
			<?php 
				$i=1;
				foreach($events as $event) { 
					if($i>3) break;
					else {
			?>
			<div class=" col s12 m6 l4">
				<div class="card medium hoverable">
					<div class="card-image waves-effect waves-block waves-light crop-img">
						<img class="activator image-height" src="<?php echo base_url($event['event_image']); ?>">
					</div>
					<div class="card-content">
						<span class="card-title activator grey-text text-darken-4"><?php echo $event['event_name'] ?><i class="material-icons right">more_vert</i></span>
					</div>
					<div class="card-reveal">
						<div style="height: 90%; word-wrap: break-word;">
							<span class="card-title grey-text text-darken-4"><?php echo $event['event_name'] ?><i class="material-icons right">close</i></span>
							<p><?php echo $event['event_description'] ?></p>
							<small><p><?php echo $event['start_event'] ?></p>
							<p><?php echo $event['end_event'] ?></p></small>
						</div>
					</div>
				</div>
			</div>
			<?php } $i++; } ?>
		</div>
		<!-- View All Events Button -->
		<div class="row">
			<div class="col s12 m12 l12">
				<center><a href="<?php echo base_url('index.php/pages/events'); ?>" class="red lighten-2 waves-effect waves-light btn">View all events</a></center>
			</div>
		</div>
		<!-- Activities preview -->
		<div class="row">
			<h4>Activities</h4>
			<?php 
				$i=1;
				foreach($activities as $activity) { 
					if($i>3) break;
					else {
			?>
			<div class=" col s12 m6 l4">
				<div class="card medium hoverable">
					<div class="card-image waves-effect waves-block waves-light crop-img">
						<img class="activator image-height" src="<?php echo base_url($activity['activity_image']); ?>">
					</div>
					<div class="card-content">
						<span class="card-title activator grey-text text-darken-4"><?php echo $activity['activity_title'] ?><i class="material-icons right">more_vert</i></span>
					</div>
					<div class="card-reveal">
						<div style="height: 90% ">
							<span class="card-title grey-text text-darken-4"><?php echo $activity['activity_title'] ?><i class="material-icons right">close</i></span>
							<p><?php echo $activity['activity_description'] ?></p>
						</div>
					</div>
				</div>
			</div>
			<?php } $i++; } ?>
		</div>
		<!-- View All Events Button -->
		<div class="row">
			<div class="col s12 m12 l12">
				<center><a href="<?php echo base_url('index.php/pages/activities'); ?>" class="red lighten-2 waves-effect waves-light btn">View all activities</a></center>
			</div>
		</div>
	</div>
</div>