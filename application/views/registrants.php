  <div class="row" style="padding-top: 100px">    
    <div class="col s12 m10 l10 offset-m1 offset-l1">
    	<table class="highlight">
        <thead>
          <tr>
              <th data-field="id">ID</th>
              <th data-field="name">Name</th>
              <th data-field="gender">Gender</th>
              <th data-field="bdate">Birthdate</th>
              <th data-field="occupation">Occupation</th>
              <th data-field="cnumber">Contact Number</th>
              <th data-field="email">Email</th>
          </tr>
        </thead>
        <tbody>
          <?php for($x = 0; $x < count($registrant); $x++) {
          	echo "<tr>";
          	echo "<td>" . $registrant[$x]['registrant_id'] . "</td>"; 
          	echo "<td>" . $registrant[$x]['last_name'] . ", " . $registrant[$x]['first_name'] . " ". $registrant[$x]['middle_name'][0]. ".</td>";
          	echo "<td>" . $registrant[$x]['gender'] . "</td>"; 
          	echo "<td>" . date('F j, Y', strtotime($registrant[$x]['birthdate'])) . "</td>"; 
            echo "<td>" . $registrant[$x]['occupation'] . "</td>";
          	echo "<td>" . $registrant[$x]['contact_number'] . "</td>"; 
          	echo "<td>" . $registrant[$x]['email'] . "</td>";
          	echo "</tr>";
          } ?>
        </tbody>
      </table>
    </div>
  </div>
