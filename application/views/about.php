<div class="row" style="margin-top: 100px">
	<div class="container row col s12 m10 l10 offset-m1 offset-l1">
		<h4>About Us</h4>
		<hr><br><br>
		<div class="row col s12 m6 l6">

			<div class="col s10 m8 l8 offset-s1 offset-m2 offset-l2">
				<h5>Mission</h5>
				<div>
					<?php echo $about['mission']; ?>
				</div>
			</div>
		</div>
		<div class="row col s12 m6 l6">
			<div class="col s10 m8 l8 offset-s1 offset-m2 offset-l2">
				<h5>Vision</h5>
				<div>
					<?php echo $about['vision']; ?>
				</div>
			</div>	
		</div>
	</div>
	<div class="row hide-on-small-and-down">
		<div class="col s12 m12 l12" style="height: 50px">
			
		</div>
	</div>
	<div class="container row col s12 m10 l10 offset-m1 offset-l1">
		<div class="row col s12 m4 l4">
			<div class="col s10 m10 l10 offset-s1 offset-m1 offset-l1">
				<h5>Email Address</h5>
				<div>
					<?php echo $about['email_address']; ?>
				</div>
			</div>
		</div>
		<div class="row col s12 m4 l4">
			<div class="col s10 m10 l10 offset-s1 offset-m1 offset-l1">
				<h5>Contact Us</h5>
				<div>
					<?php
						foreach($contacts as $contact){
							echo $contact['district'] . ' ' . $contact['number'] . '<br>';
						}
					?>
				</div>
			</div>	
		</div>
		<div class="row col s12 m4 l4">
			<div class="col s10 m10 l10 offset-s1 offset-m1 offset-l1">
				<h5>Facebook Page</h5>
				<div>
					<?php echo '<a href="' . $about['facebook_page'] . '">Altarejos Agency Facebook Page</a>'; ?>
				</div>
			</div>	
		</div>
	</div>
	<?php if($admin == 1) { ?>
		<div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
		    <a id="update-about-float"
		       data-target="modal-update"
		       data-action="<?php echo base_url('index.php/pages/updateAbout'); ?>"
		       class="update-about-btn modal-trigger btn-floating btn-large red">
		      <i class="large material-icons">edit</i>
		    </a>
		</div>
	<?php } ?>

	<!-- Modal Structure -->
	<div id="modal-update" class="modal modal-fixed-footer">
		<div class="modal-content">
			<h4 id="modal-header-title">Edit About</h4>
			<hr>
			<form id="update-about-form" method="post" action="<?php echo base_url('index.php/pages/updateAbout'); ?>" enctype="multipart/form-data">
				<div class="row">
					<div class="input-field col s12">
						<input id="mission" type="text" name="mission" value="<?php echo $about['mission']; ?>">
						<label for="mission">Mission</label>
					</div>
					<div class="input-field col s12">
						<input id="vision" type="text" name="vision" value="<?php echo $about['vision']; ?>">
						<label for="vision">Vision</label>
					</div>
					<div class="input-field col s12">
						<input id="email_address" type="text" name="email_address" value="<?php echo $about['email_address']; ?>">
						<label for="email_address">Email Address</label>
					</div>
					<div class="input-field col s12">
						<input id="facebook" type="text" name="facebook" value="<?php echo $about['facebook_page']; ?>">
						<label for="facebook">Facebook Page</label>
					</div>
				</div>
			</form>
		</div>
		<div class="modal-footer">
      		<a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat">Close</a>
      		<a href="#!" id="update-about-btn" class=" modal-action modal-close waves-effect waves-green btn-flat">Update</a>
    	</div>
	</div>
</div>