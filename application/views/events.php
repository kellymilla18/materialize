<div class="row" style="margin-top: 100px">
  <div class="col s12 m10 l10 offset-l1 offset-m1">
    <div class="row">

    <?php for($x = 0; $x < count($events); $x++) { ?>
      <div class=" col s12 m6 l4">
        <div class="card medium hoverable">
          <div class="card-image waves-effect waves-block waves-light crop-img">
            <img class="activator image-height" src="<?php echo base_url($events[$x]['event_image']); ?>">
          </div>
          <div class="card-content">
            <span class="card-title activator grey-text text-darken-4"><?php echo $events[$x]['event_name'] ?><i class="material-icons right">more_vert</i></span>
          </div>
          <div class="card-reveal">
            <div style="height: 90%; word-wrap: break-word;">
              <span class="card-title grey-text text-darken-4"><?php echo $events[$x]['event_name'] ?><i class="material-icons right">close</i></span>
              <p><?php echo $events[$x]['event_desc'] ?></p>
              <small><p><?php echo $events[$x]['start_date'] ?></p>
              <p><?php echo $events[$x]['end_date'] ?></p></small>
            </div>
            <?php if($admin == 1) { ?>
            <div class="row">
              <div class="row col s6 m6 l6 center">
                <a data-target="modal-add" 
                   data-eventid="<?php echo $events[$x]['event_id']; ?>"
                   data-eventname ="<?php echo $events[$x]['event_name']; ?>"
                   data-eventdesc ="<?php echo $events[$x]['event_desc']; ?>"
                   data-startdate ="<?php echo $events[$x]['start_date']; ?>"
                   data-enddate ="<?php echo $events[$x]['end_date']; ?>"
                   data-action ="<?php echo base_url('index.php/pages/updateevent') ?>"
                   class="edit-event-btn col s12 m12 l12  modal-trigger waves-effect waves-green btn-flat">
                  <i class="small material-icons">edit</i>
                  Edit
                </a>
              </div>
              <div class="row col s6 m6 l6 center"> 
                <a data-target="modal-delete"  data-eventid="<?php echo $events[$x]['event_id']; ?>" class="delete-act-btn col s12 m12 l12 modal-trigger waves-effect waves-green btn-flat">
                  <i class="small material-icons">delete</i>
                  Delete
                </a>
              </div>
            </div>
            <?php } ?>
          </div>
        </div>
      </div>
    <?php } ?>
      
    </div>
  </div>
</div>

  <?php if($admin == 1) { ?>

  <div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
    <a id="add-event-float"
       data-target="modal-add"
       data-action="<?php echo base_url('index.php/pages/addevent'); ?>"
       class="btn modal-trigger btn-floating btn-large red">
      <i class="large material-icons">add</i>
    </a>
  </div>


  <!-- Modal Structure -->
  <div id="modal-add" class="modal modal-fixed-footer">
    <div class="modal-content">
      <h4 id="modal-header-title">Add event</h4>
      <hr>
      <form id="add-event-form" method="post" action="<?php echo base_url('index.php/pages/addevent'); ?>" enctype="multipart/form-data">
        <input id="event-id" type="text" name="event_id" value="" hidden>
        <div class="row">
            <div class="input-field col s12">
              <input id="ev-name" type="text" name="event_name">
              <label for="ev-name">Event Name</label>
            </div>
          </div>
          <div class="file-field input-field">
            <div class="btn">
              <span>Image</span>
              <input type="file" name="piks" accept="image/*">
            </div>
            <div class="file-path-wrapper">
              <input class="file-path" type="text" name="file_path">
            </div>
          </div>
          <div class="row">
            <div class="input-field col s12">
              <textarea id="ev-desc" type="text" name="event_desc" class="materialize-textarea"></textarea>
              <label for="ev-desc">Event Description</label>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s12">
              <input id="ev-start-date" type="date" class="datepicker" name="start_date">
              <label for="ev-start-date">Start Date</label>
            </div>
          </div>

          <div class="row">
            <div class="input-field col s12">
              <input id="ev-end-date" type="date" class="datepicker" name="end_date">
              <label for="ev-end-date">End Date</label>
            </div>
          </div>
      </form>
    </div>
    <div class="modal-footer">
      <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat">Close</a>
      <a href="#!" id="add-event-btn" class=" modal-action modal-close waves-effect waves-green btn-flat">Add</a>
    </div>
  </div>

  <div id="modal-delete" class="modal">
    <div class="modal-content">
      <h6>Notice</h6>
      <hr>
      <form id="delete-event-form" action="<?php echo base_url('index.php/pages/deleteevent') ?>" method="post">
        <input type="text" id="del-act-id" name="delete-act-id" hidden value="">
      </form>
      Are you sure you want to delete this event?
    </div>
    <div class="modal-footer">
      <a class=" modal-action modal-close waves-effect waves-green btn-flat">Cancel</a>
      <a id="delete-confirm-btn" class=" modal-action modal-close waves-effect waves-green btn-flat">Delete</a>
    </div>
  </div>
  <?php } ?>