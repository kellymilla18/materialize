<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pages extends CI_Controller {
	public function index() {
		redirect(base_url('index.php/pages/home'));	
	}

	public function home() {
		$sess['admin'] = $this->session->userData('is_logged_in');
		$sess['events'] = $this->event_model->latest();
		$sess['activities'] = $this->activity_model->latest();

		$this->load->view('templates/header');
		$this->load->view('templates/navbar', $sess);
		$this->load->view('home');
		$this->load->view('templates/footer');
	}

	public function login() {
		$sess['admin'] = $this->session->userData('is_logged_in');

		$this->load->view('templates/header');
		$this->load->view('templates/navbar', $sess);
		$this->load->view('login');
		$this->load->view('templates/footer');	
	}

	public function logout() {
		$this->session->sess_destroy();
		redirect(base_url('index.php/pages/login'));
	}

	public function events () {
		$sess['admin'] = $this->session->userdata('is_logged_in');
		
		$this->load->view('templates/header');
		$this->load->view('templates/navbar', $sess);

		$x = 0;
		$data['events'] = array();
		$events = $this->event_model->getEvents();
		foreach($events->result() as $event) {
			$data['events'][$x]['event_id'] = $event->event_id;
			$data['events'][$x]['event_name'] = $event->event_name;
			$data['events'][$x]['event_desc'] = $event->event_description;
			$data['events'][$x]['start_date'] = $event->start_event;
			$data['events'][$x]['end_date'] = $event->end_event;
			$data['events'][$x]['event_image'] = $event->event_image;
			$x++;
		}


		$this->load->view('events', $data);
		$this->load->view('templates/footer');	
	}

	public function activities() {
		$sess['admin'] = $this->session->userData('is_logged_in');
		
		$this->load->view('templates/header');
		$this->load->view('templates/navbar', $sess);

		$activities = $this->activity_model->getActivities();
		$x = 0;
		$data['activity'] = array();
		foreach ($activities->result() as $activity) {
			$data['activity'][$x]['activity_id'] = $activity->activity_id;
			$data['activity'][$x]['activity_title'] = $activity->activity_title;
			$data['activity'][$x]['activity_desc'] = $activity->activity_description;
			$data['activity'][$x]['activity_image'] = $activity->activity_image;
			$x++;
		}

		$this->load->view('activities', $data);
		$this->load->view('templates/footer');	
	}

	public function about() {
		$sess['admin'] = $this->session->userData('is_logged_in');
		$sess['about'] = $this->about_model->getAbout();
		$sess['contacts'] = $this->about_model->getContact();
		
		$this->load->view('templates/header');
		$this->load->view('templates/navbar', $sess);
		$this->load->view('about');
		$this->load->view('templates/footer');	
	}

	public function validLogin() {
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		if($this->user_model->validCredentials($username, $password)) {
			$data_session = array(
				'is_logged_in' => 1
			);
			$this->session->set_userdata($data_session);
			echo "VALID";
		}
		else
			echo "INVALID";
	}

	public function addevent() {
		if(!file_exists($_SERVER['DOCUMENT_ROOT'] . '/materialize/assets/images/events'))
			mkdir($_SERVER['DOCUMENT_ROOT'] . '/materialize/assets/images/events', 0777, true);
		$eventName    = $this->input->post('event_name');
		$eventPicture = $_FILES["piks"];
		$eventDesc    = $this->input->post('event_desc');
		$startEvent   = $this->input->post('start_date');
		$endEvent     = $this->input->post('end_date');
		
		$data = array(
			'event_name'        => $eventName,
			'event_image'       => '',
			'event_description' => $eventDesc,
			'start_event'       => $startEvent,
			'end_event'         => $endEvent,
			'time_stamp'		=> date('y-m-d H:i:s')
		);
		$event_id = $this->event_model->addEvent($data);

		$fileExt      = pathinfo($eventPicture["name"], PATHINFO_EXTENSION);
		$recCount     = $this->event_model->countEvents()+1;
		$newName      = "events_".$event_id.'.'.$fileExt;
		$filePath     = $_SERVER['DOCUMENT_ROOT'] . '/materialize/assets/images/events/' . $newName;
		$upload		  = 'assets/images/events/' . $newName;
		move_uploaded_file($eventPicture['tmp_name'], $filePath);

		$this->event_model->updateEventImage($event_id, $upload);

		redirect(base_url('index.php/pages/events'));
	}

	public function updateevent() {
		if(!file_exists($_SERVER['DOCUMENT_ROOT'] . '/materialize/assets/images/events'))
			mkdir($_SERVER['DOCUMENT_ROOT'] . '/materialize/assets/images/events', 0777, true);
		$event_id = $this->input->post('event_id');
		$eventName    = $this->input->post('event_name');
		$eventDesc    = $this->input->post('event_desc');
		$startEvent   = $this->input->post('start_date');
		$endEvent     = $this->input->post('end_date');

		$data = array(
			'event_name'        => $eventName,
			'event_description' => $eventDesc,
			'start_event'       => $startEvent,
			'end_event'         => $endEvent
		);

		if(!(isset($_FILES['piks']) && count($_FILES['piks']['error']) == 1 && $_FILES['piks']['error'][0] > 0) && isset($_FILES['piks'])) {
			$eventPicture = $_FILES["piks"];
			$fileExt      = pathinfo($eventPicture["name"], PATHINFO_EXTENSION);
			$newName      = "events_".$event_id.'.'.$fileExt;
			$filePath     = $_SERVER['DOCUMENT_ROOT'] . '/materialize/assets/images/events/' . $newName;
			if($fileExt == 'jpg' || $fileExt == 'jpeg' || $fileExt == 'png' || $fileExt == 'ico' || $fileExt == 'gif') {
				if(file_exists($filePath))
					unlink($filePath);
				$upload		  = 'assets/images/events/' . $newName;
				$data['event_image'] = $upload;
				move_uploaded_file($eventPicture['tmp_name'], $filePath);
			}
		}
		$this->event_model->updateEvent($event_id, $data);
		redirect(base_url('index.php/pages/events'));
	}

	public function addactivity() {
		if(!file_exists($_SERVER['DOCUMENT_ROOT'] . '/materialize/assets/images/activities'))
			mkdir($_SERVER['DOCUMENT_ROOT'] . '/materialize/assets/images/activities', 0777, true);
		$activityName 		 = $this->input->post('activity_name');
		$activityDescription = $this->input->post('activity_desc');
		
		$data = array(
			'activity_title'	   => $activityName,
			'activity_description' => $activityDescription,
			'activity_image'	   => '',
			'time_stamp'		   => date('y-m-d H:i:s')
		);
		$activity_id = $this->activity_model->addActivities($data);

		$activityPic		 = $_FILES['piks'];
		$fileExt			 = pathinfo($activityPic["name"], PATHINFO_EXTENSION);
		$newName			 = "activity_".$activity_id.'_0.'.$fileExt;
		$filePath			 = $_SERVER['DOCUMENT_ROOT'] . '/materialize/assets/images/activities/' . $newName;
		$upload				 = 'assets/images/activities/' . $newName;
		move_uploaded_file($activityPic['tmp_name'], $filePath);

		$this->activity_model->updateactivityimage($activity_id, $upload);

		$repo['activity_id'] = $activity_id;
		$repo['activity_image'] = $upload;
		$this->activity_model->addtorepo($repo);
		redirect(base_url('index.php/pages/activities'));
	}

	public function updateactivity() {
		if(!file_exists($_SERVER['DOCUMENT_ROOT'] . '/materialize/assets/images/activities'))
			mkdir($_SERVER['DOCUMENT_ROOT'] . '/materialize/assets/images/activities', 0777, true);
		$activityId          = $this->input->post('activity_id');
		$activityName 		 = $this->input->post('activity_name');
		$activityDescription = $this->input->post('activity_desc');
		
		$data = array(
			'activity_title'	   => $activityName,
			'activity_description' => $activityDescription,
		);

		if(!(isset($_FILES['piks']) && count($_FILES['piks']['error']) == 1 && $_FILES['piks']['error'][0] > 0) && isset($_FILES['piks'])) {
			$activityPic		 = $_FILES['piks'];
			$fileExt			 = pathinfo($activityPic["name"], PATHINFO_EXTENSION);
			$recCount			 = $this->activity_model->countActivities()+1;
			if($fileExt == 'jpg' || $fileExt == 'jpeg' || $fileExt == 'png' || $fileExt == 'ico' || $fileExt == 'gif') {
				$newName			 = "activity_".$recCount.'.'.$fileExt;
				$filePath			 = $_SERVER['DOCUMENT_ROOT'] . '/materialize/assets/images/activities/' . $newName;
				
				$copy_path = $filePath;
				if(file_exists($filePath))
					unlink($filePath);
				$upload				 = 'assets/images/activities/' . $newName;
				$data['activity_image'] = $upload;
				move_uploaded_file($activityPic['tmp_name'], $copy_path);
			}
		}
		$this->activity_model->updateActivities($activityId, $data);
		redirect(base_url('index.php/pages/activities'));
	}

	public function deleteevent() {
		$event_id = $this->input->post('delete-act-id');
		$this->event_model->deleteevent($event_id);
		redirect(base_url('index.php/pages/events'));
	}

	public function deleteactivity() {
		$activity_id = $this->input->post('delete-activity-id');
		$this->activity_model->deleteactivity($activity_id);
		redirect(base_url('index.php/pages/activities'));		
	}

	public function updateAbout(){
		$data = array(
			'mission'       => $this->input->post('mission'),
			'vision'        => $this->input->post('vision'),
			'email_address' => $this->input->post('email_address'),
			'facebook_page' => $this->input->post('facebook')
		);
		$ret = $this->about_model->updateAbout($data);
		if($ret)
			redirect(base_url('index.php/pages/about'));
	}

	public function register() {
		$sess['admin'] = $this->session->userData('is_logged_in');

		$this->load->view('templates/header');
		$this->load->view('templates/navbar', $sess);
		$this->load->view('register');
		$this->load->view('templates/footer');	
	}

	public function registerAttempt(){
		$data = array(
			'first_name' => $this->input->post('first_name'),
			'middle_name' => $this->input->post('middle_name'),
			'last_name' => $this->input->post('last_name'),
			'contact_number' => $this->input->post('contact_number'),
			'sex' => $this->input->post('gender'),
			'email_address' => $this->input->post('email'),
			'birthdate' => date('Y-m-d', strtotime($this->input->post('birthdate'))),
			'occupation' => $this->input->post('occupation'),
			'interviewed' => 0
		);
		$this->registrant_model->addRegistrant($data);
		redirect(base_url('index.php/pages/home'));
	}

	public function registrants() {
		$sess['admin'] = $this->session->userData('is_logged_in');

		$this->load->view('templates/header');
		$this->load->view('templates/navbar', $sess);

		$x = 0;
		$data['registrant'] = array();
		$registrants = $this->registrant_model->getRegistrants();
		foreach ($registrants->result() as $registrant) {
			$data['registrant'][$x]['registrant_id'] = $registrant->registrant_id;
			$data['registrant'][$x]['first_name'] = $registrant->first_name;
			$data['registrant'][$x]['middle_name'] = $registrant->middle_name;
			$data['registrant'][$x]['last_name'] = $registrant->last_name;
			$data['registrant'][$x]['contact_number'] = $registrant->contact_number;
			$data['registrant'][$x]['gender'] = $registrant->sex;
			$data['registrant'][$x]['birthdate'] = $registrant->birthdate;
			$data['registrant'][$x]['email'] = $registrant->email_address;
			$data['registrant'][$x]['interviewed'] = $registrant->interviewed;
			$data['registrant'][$x]['occupation'] = $registrant->occupation;
			$x++;
		}
		$this->load->view('registrants', $data);
		$this->load->view('templates/footer');
	
	}

	public function viewactivity($activity_id) {
		$data['images'] = $this->activity_model->activityByID($activity_id);
		$data['detail'] = $this->activity_model->getActivityByID($activity_id);
		$sess['admin'] = $this->session->userData('is_logged_in');
		$this->load->view('templates/header');
		$this->load->view('templates/navbar', $sess);
		$this->load->view('viewactivity', $data);
		$this->load->view('templates/footer');
	}
	
	public function imagerepo($activity_id) {
		$activityPic		 = $_FILES['piks'];
		for($x = 0; $x < count($activityPic["name"]); $x++) {
			$fileExt			 = pathinfo($activityPic["name"][$x], PATHINFO_EXTENSION);
			if($fileExt == 'jpg' || $fileExt == 'jpeg' || $fileExt == 'png' || $fileExt == 'ico' || $fileExt == 'gif') {
				$id       = $this->activity_model->addImage($activity_id);

				$newName  = "activity_" . $activity_id . "_" . $id . "." . $fileExt;
				$filePath = $_SERVER['DOCUMENT_ROOT'] . '/materialize/assets/images/activities/' . $newName;				
				$upload   = 'assets/images/activities/' . $newName;
				move_uploaded_file($activityPic['tmp_name'][$x], $filePath);

				$this->activity_model->updateImage($activity_id, $id, $upload);
			}
		}
		redirect(base_url("index.php/pages/viewactivity/$activity_id"));
	}
}