$('.carousel').carousel();
$('.slider').slider({full_width: true});
$('.modal-trigger').leanModal();

$(window).scroll(function () {
//  console.log($(window).scrollTop());
  if ($(window).scrollTop() > 263) {
    $('.navi-top').addClass('nav-fixed');
  }
  if ($(window).scrollTop() < 264) {
    $('.navi-top').removeClass('nav-fixed');
  }
});

$('.collapsible').collapsible();

$('#add-event-btn').on('click', function() {
  $('#add-event-form').submit();
});

$('#update-about-btn').on('click', function() {
  $('#update-about-form').submit();
});

$(".button-collapse").sideNav();

$('#add-activity-btn').on('click', function() {
  $('#add-activity-form').submit();
});

$('.datepicker').pickadate({
    selectMonths: true, // Creates a dropdown to control month
    selectYears: 300 // Creates a dropdown of 15 years to control year
  });

$('#login-form').submit(function(e) {
  e.preventDefault();
  e.stopImmediatePropagation();
  var formData = new FormData(this);
  $.ajax({
    url: "http://" + window.location.hostname + "/materialize/index.php/pages/validLogin",
    type: "post",
    data: formData,
    contentType: false,
    processData: false,
    async: false,
    success: function(datapass) {
      if(datapass == "INVALID") {
        Materialize.toast('Invalid Credentials', 4000);
      } else {
        window.location.href = 'http://' + window.location.hostname + '/materialize/index.php/pages/home';
      }
    }
  });
  return false;
});

$('.delete-act-btn').on('click', function() {
  $("#del-act-id").val($(this).data('eventid'));
});

$('#delete-confirm-btn').on('click', function() {
  $('#delete-event-form').submit();
});

$('.edit-event-btn').on('click', function(e) {
  $('input[name="event_name"]').val($(this).data('eventname'));
  $('textarea[name="event_desc"]').val($(this).data('eventdesc'));
  $('input[name="start_date"]').val($(this).data('startdate'));
  $('input[name="end_date"]').val($(this).data('enddate'));
  $('input[name="event_id"]').val($(this).data('eventid'));

  $('label').addClass('active');
  $('#add-event-form').attr("action", $(this).data("action"));
  $('#modal-header-title').html('Edit Event');
  $('#add-event-btn').html('Save');
});

$('#add-event-float').on('click', function() {
  $('input[name="event_name"]').val('');
  $('textarea[name="event_desc"]').val('');
  $('input[name="start_date"]').val('');
  $('input[name="end_date"]').val('');
  $('input[name="event_id"]').val('0');

  $('#add-event-form').attr("action", $(this).data("action"));
  $('#modal-header-title').html('Add Event');
  $('label').removeClass('active');
  $('#add-event-btn').html('Add');
});

$('.delete-activity-btn').on('click', function() {
  $('input[name="delete-activity-id"]').val($(this).data('actid'));
});

$('#delete-actconfirm-btn').on('click', function() {
  $('#delete-activity-form').submit();
});

$('.edit-activity-btn').on('click', function() {
  $('.modal-activity-header').html('Edit Activity');
  $('#add-activity-btn').html('Save');
  $('input[name="activity_name"]').val($(this).data('activitytitle'));
  $('input[name="activity_id"]').val($(this).data('activityid'));
  $('textarea[name="activity_desc"]').val($(this).data('activitydesc'));
  $('#add-activity-form').attr('action', $(this).data('action'));

  $('label').addClass('active');
});

$('#add-activity-float').on('click', function() {
  $('.modal-activity-header').html('Add Activity');
  $('#add-activity-btn').html('Add');
  $('input[name="activity_name"]').val('');
  $('input[name="activity_id"]').val('');
  $('textarea[name="activity_desc"]').val('');
  $('#add-activity-form').attr('action', $(this).data('action'));

  $('label').removeClass('active');  
});

$('#register-btn').on('click', function() {
  $('#register-form').submit();
});

$('#add-finalize').on('click', function() {
  $('#add-img-form').submit();
});